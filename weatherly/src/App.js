import { useState } from "react";
import "./App.css";

function App() {
  const [citySearch, onChangeSearchCity] = useState("");
  const [cityList, addCityList] = useState([]);
  const [searchDataLogs, addSearchDataLogs] = useState([]);
  const [err, setError] = useState(null);

  const onSubmit = () => {
    if (citySearch !== "" && !cityList.includes(citySearch)) {
      fetch(`https://weatherly-application.herokuapp.com/city/${citySearch}`)
        .then((o) => {
          if (o.status === 200) return o.json();
          else {
            setError("Does not exist!");
            return "Error";
          }
        })
        .then((o) => {
          if (o !== "Error") {
            console.log("HELLO");
            addCityList([...cityList, citySearch]);
            addSearchDataLogs([...searchDataLogs, o]);
            onChangeSearchCity("");
            setError(null);
          }
        })
        .catch(err, () => {
          setError(err);
          onChangeSearchCity("");
        });
    }
  };

  return (
    <div className="full-container">
      <div className="header-container">
        <h1 className="name">Weatherly.</h1>
      </div>
      <div className="body-container">
        <div className="searchbox">
          <input
            className="searchbar"
            onChange={(e) => onChangeSearchCity(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                onSubmit();
              }
            }}
            value={citySearch}
            placeholder="Search for a city name"
          />
          <button className="searchbutton" onClick={onSubmit}>
            GO
          </button>
          {err && <div className="error">{err}</div>}
        </div>

        <ul className="results-container">
          {searchDataLogs.length === 0 && <div>Search for a city!</div>}
          {searchDataLogs.map((o, idx) => {
            return (
              <li className="weather-listItem" key={idx}>
                <h3>
                  {o.city_name}, {o.country} - {o.time}
                </h3>
                <div>{o.description}</div>
                <div>{(o.temperature - 273.15).toFixed(2)}°C</div>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}

// "3ab66bad541451921f55ffbcd9c892ec"

export default App;
